# Yumu API User Guide


## @deprecated

## Overview

Best viewed in Chrome with "Markdown Preview" plugin.


## Use cases

User

* users login with facebook/google+
* users can create an activity
* users can join or follow activities
* owner user can disable an activity
* owner user can disable an offensive post
* users can upvote/downvote/pin/report and activity or post. User clout (Ranking) grows due to these actions. Clout depends on votes, pins, memberships, connections etc.
* member users can create a post inside an acitivity
* Users can decide what to share with others

Activities/Posts/Activity Stream (AS)

* AS defaults to location based
* AS is made up of stream of elements. Elements can be of type activity, post, user actions, and, anything else. For MVP AS is made up of only activities.
* Guests see only public elements
* AS can be of multiple kind: My activities, group acitvities, this weekend, and discussions
* Pinned elements will show up in My activities
* Logged in user can create an activity
* Activity can be public or private (TBD)
* Template based activity creation. MVP will have few templates in the form of metadata
* Users with higher clout can create templates (not for MVP)
* Activities have properties like: name, date, location, privacy, pictures, hashtag, address, # of members, and repetition.
* Element can be shared on Facebook/Google+
* Element CRUD operations should store location
* Activity owner can invite other users
* Activity can have more than one owner. How?
* Terms and conditions should be shown during activity creation
* Activities will have conversations (made up of posts)
* Activity will have a rating score based on an algorithm
* Member users can invite other friends for an activity
* Owner users can approve membership requests
* Owner users can make an activity public/private


Search

* list activities by location
* list activities created by someone
* list activities for which someone is a member
* list activities for which someone is a follower
* list users

Notifications

* Friend request from users
* Membership request to users
* Membership request from users

Profile

* My profile will have privacy settings
* My profile will show notifications where users can act on it
* Other profile will show only public elements of the other user
* Other profile will show list of events where user is a member
* Other profile will show list of events where user is a follower


## Top level REST resources
Users
Activities
Posts
Search
Profile
Social actions
Admin actions
Notification
Ranking
Template

Note: All top-level resources will have a common meta data object. This will be useful for AS for abstraction.

## API Details

|ID|HTTP Method|Resource|Description|
|---|---|---|---|
|users.create|`POST`|`/v1/users`| Add user to a collection. Avoid duplicates|
|users.list|`GET`|`/v1/users`| Is this needed?|
|users.list_by_id|`GET`|`/v1/users/{id}`| Get details about particular user|
|templates.list|`GET`| `/v1/templates`| List templates for activity creation|
|activities.create|`POST`| `/v1/core/activities`| Create an activity|
|activities.update|`PUT`| `v1/core/activities/{id} {"action":"upvote/downvote/report/share/pin/join/follow/share"}`| Update an activity|
|activities.list_by_access_type|`GET`| `/v1/users/{id}/activities?access_type={author/member/follower}`| Output AS depends on who is calling (SecurityContext). Could be "My activities" or "some user activities"|
|activities.list|`GET`| `/v1/core/activities`| Default AS. Depends on location in request header|
|activities.list_by_id|`GET`| `/v1/core/activities/{id}`| Get a specific activity. For now, there are no filters.|
|activities.list_by_location|`GET`| `/v1/core/activities?location=lat:lon`| Filter by location. How different is it from default activities.list?|
|activities.list_by_time|`GET`| `/v1/core/activities?timerange=weekend`| Filter by time and location passed in header|
|activities.list_users| `GET`| `/v1/core/activities/{id}/users?type={friend/all}`|Get users or friends in this activity|
|activities.delete|`DELETE`| `/v1/core/activities`| Delete an activity. Only admins or activity owners can invoke this api|
|activities.list_posts|`GET`| `/v1/core/activities/{id}/posts`| Get all posts under an activity|
|activities.create_post|`POST`| `/v1/core/activities/{id}/posts`| Add a post to conversation in an activity|
|posts.update|`PUT`| `v1/core/posts/{id} {"action":"upvote/downvote/report/share/pin/join/follow/share"}`| Update an activity|
|posts.list|`GET`| `/v1/core/posts`| Discussions. Use case TBD. Filters TBD|
|posts.delete|`DELETE`| `/v1/core/posts/{id}`| Delete a an offensive post. Only admin or activity owner can invoke|
|links.create|`POST`| `/v1/links {"type":"friend/follow/join", source": "me/activity", "destination":"user/activity"}` | create a new link between user or activity|
|links.update|`PUT`| `/v1/links {"action":"approve/deny/ignore/block"}` | act on a link request|
|links.list_friends|`GET`| `/v1/links?type=friend` | get list of one's friends.|
|search.list|`GET`| `/v1/search?q={search_term}`| TBD: Search api depends on search engine. What are we searching?|
|admin.do|`POST`| `/v1/admin/TBD`| TBD: Admin actions|
|notifications.list|`GET`| `/v1/notifications`| Get all notifications for a user. Depends on who is the caller|
|notifications.update|`PUT`| `/v1/notifications/{id}`| Act on the notification|
|systemnotice.list|`GET`| `/v1/systemnotice?type={tc/maintenance}`| List terms and conditions or maintenance notice etc|



## Application page to API mapping
|Login Page | API ID | Description|
|---|---|---|
|Create user|users.create|Bring FB/G+ user into Yumu db|
|Get user|users.list_by_id|Load user info + clout|

|Activities Stream/Landing Page | API ID | Description|
|---|---|---|
|Search|search.list|Search. TBD|
|My activities|activities.list_by_access_type|Show my activities for logged in users only|
|Activities|activities.list|Show a stream of activities|
|Weekend Activities|activities.list_by_time|Show activities happening this weekend in this locality|


|Activities Details Page | API ID | Description|
|---|---|---|
|Get single activity details|activities.list_by_id|Click to open a specific activity|
|Get conversation|activities.list_posts|Get all the discussions under this activity|
|Join Activity|links.create|Send membership request|
|Invite users|links.create|Invite users to join this activity|

|Activity Creation Page | API ID | Description|
|---|---|---|
|get templates| templates.list|get all available templates for activity creation|
|create a new activity| activities.create|create a new activity|
|show T&C|systemnotice.list| Get T&C text|
|Invite users|links.create|Invite users to join this new activity. This should may be done after activity is created|

|My Profile Page | API ID | Description|
|---|---|---|
|Get my details|users.list_by_id|Details are returned based on caller|
|Set privacy|TBD|TBD|


|Notifications Page| API ID | Description|
|---|---|---|
|Get my notifications|notifications.list||
|Act on notification|notifications.update|This should be somehow chained to another api like approve friend request. HATEOS?|


|Other User Profile Page | API ID | Description|
|---|---|---|
|Send friend request|links.create||
|Accept friend request|links.update||


|User Actions Across Pages | API ID | Description|
|---|---|---|
|Send friend request|links.create||
|Block friend|links.update| Use block option|


