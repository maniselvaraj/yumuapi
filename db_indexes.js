use yumumongodb01
db.yumuUser.createIndex({"socialInfo.name":"text", "socialInfo.email":"text"})
db.activity.createIndex({name:"text", description: "text", hashtags: "text"})
db.post.createIndex({content:"text", hashtags:"text"})
db.yumuUser.createIndex({"socialInfo.userId":1}, {unique:true})
db.activity.ensureIndex({"location.point": "2dsphere"})
db.createCollection("appLog", {"capped":true, "max":10000, "size":300000})
