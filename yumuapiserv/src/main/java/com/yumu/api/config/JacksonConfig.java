package com.yumu.api.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.geo.GeoJsonModule;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.databind.Module;


/*
 * TODO: Revisit this and fix the custom Location.java Json deserializer with GeoJsonModule
 */

@Configuration
@EnableWebMvc
public class JacksonConfig  extends WebMvcConfigurerAdapter {

	@Bean
	public Module registerGeoJsonModule(){
		System.out.println("MDEBUG registering GeoJsonModule");
		return new GeoJsonModule();
	}
	//	   
	//	   @Override
	//		public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
	//		   System.out.println("MDEBUG *****************************>>>>>>>>>>>>>>>>>>>>>>>>");
	//			Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
	//			
	//			builder.modulesToInstall(GeoJsonModule.class);
	//			
	//			//builder.indentOutput(true).dateFormat(new SimpleDateFormat("yyyy-MM-dd"));
	//			
	//			//ObjectMapper mapper = new ObjectMapper();
	//			//mapper.registerModule(new GeoJsonModule());
	//				//builder.configure(mapper);
	//			converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
	//			//converters.add(new MappingJackson2XmlHttpMessageConverter(builder.createXmlMapper(true).build()));
	//		}



}
