/*
 * Copyright (c) 2015, 2016, Smirva Systems Private Limited. All rights reserved.
 */
package com.yumu.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

//@Configuration
//@EnableMongoRepositories(basePackages = "com.yumu.data.repositories")
public class MongoDBConfig extends AbstractMongoConfiguration {

	@Override
	protected String getDatabaseName() {
		return "";//"yumumongodb01";
	}

	@Override
	public Mongo mongo() throws Exception {
		return null;// new MongoClient("127.0.0.1", 27017);
	}

    @Override
    public String getMappingBasePackage() {
        return "org.yumu.api.spec";
    }
}
