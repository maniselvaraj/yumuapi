package com.yumu.data.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.yumu.api.spec.common.Feedback;

public interface FeedbackRepository extends MongoRepository<Feedback, String>  {

}
