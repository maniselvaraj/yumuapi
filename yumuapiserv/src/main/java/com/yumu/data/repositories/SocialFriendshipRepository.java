package com.yumu.data.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.yumu.api.spec.user.SocialFriendship;

public interface SocialFriendshipRepository extends MongoRepository<SocialFriendship, String> {

}
