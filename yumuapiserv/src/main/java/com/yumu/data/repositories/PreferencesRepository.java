package com.yumu.data.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.yumu.api.spec.user.preferences.Preferences;

public interface PreferencesRepository extends MongoRepository<Preferences, String>{

	Preferences findByYumuUserId(String userId);
}
