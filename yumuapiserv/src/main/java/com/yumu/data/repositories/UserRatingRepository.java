package com.yumu.data.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.yumu.api.spec.common.UserRating;

public interface UserRatingRepository extends MongoRepository<UserRating, String> {

}

