/*
 * Copyright (c) 2015, 2016, Smirva Systems Private Limited. All rights reserved.
 */
package com.yumu.api.managers;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.yumu.api.spec.common.Phone;
import com.yumu.api.spec.common.Phone.Type;

public class UserManagerTest {

	@Test
	public void testCopyPhones() throws Exception {
		
		Method method = UserManager.class.getDeclaredMethod("copyPhones", List.class, List.class);
		method.setAccessible(true);
		
		UserManager um = new UserManager();
		
		List<Phone> oldPhones = new ArrayList<>();
		List<Phone> newPhones = new ArrayList<>();
		method.invoke(um, oldPhones, newPhones);
		Assert.assertTrue(oldPhones.size()==0);

		
		Phone p1=  new Phone(); p1.setType(Type.HOME);p1.setNumber("4088764999"); newPhones.add(p1);
		method.invoke(um, oldPhones, newPhones);
		Assert.assertTrue(oldPhones.size()==1);
		oldPhones.clear();
		
		Phone p2=  new Phone(); p2.setType(Type.MOBILE);p2.setNumber("4088764998"); newPhones.add(p2);
		method.invoke(um, oldPhones, newPhones);
		Assert.assertTrue(oldPhones.size()==2);

		newPhones.clear();
		newPhones.add(p1);
		Phone p3=  new Phone(); p3.setType(Type.MOBILE);p3.setNumber("4088764997"); newPhones.add(p3);
		method.invoke(um, oldPhones, newPhones);
		Assert.assertTrue(oldPhones.size()==2);
		
		oldPhones.forEach(p -> {
			if(p.getType()==Type.MOBILE) {
				System.out.println("Phone updated correctly");
				Assert.assertTrue(p.getNumber().equals("4088764997"));
			}
		});
		
		
	}

}
