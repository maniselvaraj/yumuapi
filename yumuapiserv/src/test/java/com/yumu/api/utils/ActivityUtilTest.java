package com.yumu.api.utils;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class ActivityUtilTest {

	@Test
	public void testExtractHashTags() {
		String text = "My name is #mani. I am from #chennai";
		Set<String> tags = ActivityUtil.extractHashTags(text);
		Assert.assertNotNull(tags);
		Assert.assertEquals(2, tags.size());
		tags.forEach(t -> {
			Assert.assertTrue(tags.contains("mani") || tags.contains("chennai"));
		});
	}

	@Test
	public void testExtractHashTagsNull() {
		String text = null;
		Set<String> tags = ActivityUtil.extractHashTags(text);
		Assert.assertNotNull(tags);
		Assert.assertEquals(0, tags.size());
	}

	@Test
	public void testExtractHashTagsEmpty() {
		String text = "";
		Set<String> tags = ActivityUtil.extractHashTags(text);
		Assert.assertNotNull(tags);
		Assert.assertEquals(0, tags.size());
	}
	
}
