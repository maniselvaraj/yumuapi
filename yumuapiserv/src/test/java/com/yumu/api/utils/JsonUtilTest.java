package com.yumu.api.utils;

import java.io.IOException;

import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.yumu.api.spec.activities.Post;
import com.yumu.api.spec.common.Location;

import junit.framework.TestCase;

public class JsonUtilTest extends TestCase {

	public void testToStringObject() {

		Location loc = new Location();
		
		GeoJsonPoint point = new GeoJsonPoint(8.0, 9.0);
		Point point2 = new Point(8.0, 9.0);
		
		loc.setPoint(point);
		
		System.out.println(JsonUtil.toString(loc));
		
		String json  = JsonUtil.toString(loc);
		
		
		try {
			Location loc2 = (Location) JsonUtil.toObject(json, "com.yumu.api.spec.common.Location");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	}

	
	public void toObjectTest() throws JsonParseException, JsonMappingException, ClassNotFoundException, IOException {
		
		String json =     "{\"point\":{\"x\":8.0,\"y\":9.0}}";
		
		Location loc = (Location) JsonUtil.toObject(json, "com.yumu.api.spec.common.Location");
	}
	
	public void testPost(){
		
		Post post = new Post();
		
		post.setActivityId("1232");
		post.setContent("this is post");
		post.setStatus(Post.Status.ACTIVE);
		
		System.out.println(JsonUtil.toString(post));
		
	}
	
}
