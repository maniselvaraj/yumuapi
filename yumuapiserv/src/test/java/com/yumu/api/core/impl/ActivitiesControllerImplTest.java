package com.yumu.api.core.impl;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import com.yumu.api.spec.common.Address;
import com.yumu.api.spec.common.Location;

public class ActivitiesControllerImplTest {

	@Test
	public void testCompareLocation() {
		
		Location one = new Location();
		GeoJsonPoint pOne = new GeoJsonPoint(1.0, 1.0);
		one.setPoint(pOne);
		Address address1 = new Address();
		address1.setCity("boston");
		address1.setCountryCode("US");
		address1.setLine1("21 Walker st");
		address1.setPostalCode("02144");
		address1.setState("MA");
		one.setAddress(address1);
		
		Location two = new Location();
		GeoJsonPoint pTwo = new GeoJsonPoint(1.0, 1.0);
		two.setPoint(pTwo);
		Address address2 = new Address();
		address2.setCity("boston");
		address2.setCountryCode("US");
		address2.setLine1("21 Walker st");
		address2.setPostalCode("02144");
		address2.setState("MA");
		two.setAddress(address2);

		
		if(one.equals(null)){
			System.out.println("EQUAL");
		} else {
			System.out.println("UNEQUAL");
		}
		
	}

}
